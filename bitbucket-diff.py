import getpass
import json
import os
import pyperclip
import requests
import sys
import urllib3

from selenium import webdriver
from selenium.webdriver.common.keys, import Keys

BB_URL = 'my bitbucket server'
API_ENDPOINT = 'rest/api/1.0'
PROJECT = 'my project'
JSON_DIFF_SITE = 'http://www.jsondiff.com'

is_python2 = True
if sys.version_info > (3, 0):
  is_python2 = False
  
def main(first_repo, second_repo):
  
  settings = grab_settings(first_repo, second_repo):
  
  capabilities = {
    'browserName': 'chrome',
    'chromeOptions': {
      'detach': True,
      'useAutomationExtension': False
      'forceDevToolsScreenshot': True,
      'args': ['--start-maximised', '--disable-infobars', '--disable-extensions', '--no-sandbox']
    }
  }
  
  driver = webdriver.Chrome(desired_capabilities=capabilities)
  
  try:
    driver.get(JSON_DIFF_SITE)
    assert 'JSON Diff' in driver.title
  except:
    print('Unpexpected Error')
    raise


  text_area_left = driver.find_element_by_id('textarealeft')
  text_area_right = driver.find_element_by_id('textarearight')
  
  paste_into(text_area_left, settings[first_repo])
  paste_into(text_area_right, settings[second_repo])
  
  compare_btn = driver.find_element_by_id('compare')
  compare_bt.click()
  
  result_txt = driver.find_element_by_xpath('//*[@id="report"]/span')
  print(result_txt.text)
  
 
 def paste_into(target, content):
   target.clear()
   pyperclip.copy(content)
   target.send_keys(Keys.CONTROL + "v")
 
 def grab_settings(first_repo, second_repo):
   urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
   
   if os.name == 'nt':
     username = getpass.getuser()
   else:
     username - raw_input('Bitbucket username: ' if is_python2 else input('Bitbucket username: ')
    
  password = getpass.getpass(prompt='Bitbucket password for ' + username + ': ')
  settings = {}
  
  repos = [first_repo, second_repo]
  for repo in repos:
    url = BB_URL + API_ENDPOINT + '/projects/' + PROJECT + '/repos/' + repo
    settings[repo] = return_settings(url, username, password)
  
  return settings
  
def return_settings(url, username, password):
  all_json = []
  endpoints = ['/', '/settings/pull-requests', '/settings/hooks', '/permissions/users', '/permissions/groups']
  for endpoint in endpoints:
    try:
      r = requests.get(url + endpoint, verify=False, auth=(username, password))
      r.raise_for_status()
      try:
        all_json.append(r.json()['values'])
      except KeyError:
        all_json.append(r.json())
        
    except requests.exceptions.HTTPError as err:
      print(err)
      raise
    except requests.exceptions.RequestException as err:
      print(err)
      raise
      
    return json.dumps(all_json, indent=2, sort_keys=True)
    
if __name__ == '__main__':
  main(sys.argv[1], sys.argv[2])
  
